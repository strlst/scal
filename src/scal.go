package main

import (
	"log"
	"os"
	"strings"

	tea "github.com/charmbracelet/bubbletea"
)

const (
	EXIT_SUCCESS int = iota
	EXIT_ENV_INVALID
	EXIT_SPECIFIED_PATH_INVALID
	EXIT_FILE_INVALID
)

func main() {
	// query program name by call
	program := os.Args[0][strings.LastIndex(os.Args[0], "/")+1 : len(os.Args[0])]

	// read and parse calendar file
	calendar := ReadCalendar(program)

	// create model out of calendar
	m := NewModel(&calendar)
	p := tea.NewProgram(m, tea.WithAltScreen())
	if _, err := p.Run(); err != nil {
		log.Fatal(err)
	}
}
