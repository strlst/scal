package main

import "github.com/charmbracelet/bubbles/textinput"

type Model struct {
	calendar     *Calendar
	command      textinput.Model
	dayOffset    int
	showDebug    bool
	windowWidth  int
	windowHeight int
}

func NewModel(calendar *Calendar) *Model {
	ti := textinput.New()
	ti.Placeholder = ": to enter command mode (:q to quit)"
	return &Model{
		calendar: calendar,
		command:  ti,
	}
}
