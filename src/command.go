package main

import (
	tea "github.com/charmbracelet/bubbletea"
)

func ProcessCommand(input string) tea.Cmd {
	var cmd tea.Cmd = nil
	if input == "q" {
		cmd = tea.Quit
	}
	return cmd
}
