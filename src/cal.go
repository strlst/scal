package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"time"
)

type Date struct {
	year  int
	month time.Month
	day   int
}

type Entry struct {
	Start     time.Time
	End       time.Time
	StartDate Date
	EndDate   Date
	Tags      []string
}

type Calendar struct {
	Entries []Entry
}

func DateFromTime(t time.Time) Date {
	year, month, day := t.Date()
	return Date{year: year, month: month, day: day}
}

// write func FilterEntries(from time.Time) []Entry

func ReadCalendar(program string) Calendar {
	// query environment
	user, userOk := os.LookupEnv("USER")
	home, homeOk := os.LookupEnv("HOME")
	if !(userOk && homeOk) {
		fmt.Fprintf(os.Stderr, "%s: error: environment variables $USER and $HOME need to be set\n", program)
		os.Exit(EXIT_ENV_INVALID)
	}

	defaultCalendarFilepath := fmt.Sprintf("%s/.local/share/%s/default.scal", home, user)

	file, err := os.Open(defaultCalendarFilepath)
	if os.IsNotExist(err) {
		fmt.Fprintf(os.Stderr, "%s: specified logs file \"%s\" does not exist\n", program, defaultCalendarFilepath)
		os.Exit(EXIT_SPECIFIED_PATH_INVALID)
	} else if err != nil {
		fmt.Fprint(os.Stderr, err)
	}

	// instantiate empty calendar at first
	calendar := Calendar{[]Entry{}}

	// simply scan all lines to construct the calendar
	scanner := bufio.NewScanner(file)
	index := 0
	for scanner.Scan() {
		line := scanner.Text()
		fields := strings.Split(line, " ")

		// check basic property of line
		if len(fields) < 4 {
			fmt.Fprintf(os.Stderr, "%s: calendar file could not be parsed at line %d: \"%s\" invalid\n", program, index, line)
			os.Exit(EXIT_FILE_INVALID)
		}

		start, err1 := time.Parse(time.DateTime, fmt.Sprintf("%s %s", fields[0], fields[1]))
		end, err2 := time.Parse(time.DateTime, fmt.Sprintf("%s %s", fields[0], fields[2]))
		tags := fields[3:]

		if err1 != nil || err2 != nil {
			fmt.Fprintf(os.Stderr, "%s: calendar file could not be parsed at line %d: \"%s\" time conversion error\n", program, index, line)
			os.Exit(EXIT_FILE_INVALID)
		}

		// create entry
		entry := Entry{
			Start:     start,
			End:       end,
			StartDate: DateFromTime(start),
			EndDate:   DateFromTime(end),
			Tags:      tags,
		}

		// store entries
		calendar.Entries = append(calendar.Entries, entry)

		// keep track of line numbers
		index++
	}

	return calendar
}
