package main

import (
	"strings"
	"time"

	tea "github.com/charmbracelet/bubbletea"
)

const (
	STYLE_RESET        = "\033[0m"
	STYLE_BOLD         = "\033[1m"
	STYLE_UNDERLINE    = "\033[4m"
	STYLE_STRIKE       = "\033[9m"
	STYLE_ITALIC       = "\033[3m"
	COLOR_RED          = "\033[0;31m"
	COLOR_GREEN        = "\033[0;32m"
	COLOR_YELLOW       = "\033[0;33m"
	COLOR_BLUE         = "\033[0;34m"
	COLOR_PURPLE       = "\033[0;35m"
	COLOR_CYAN         = "\033[0;36m"
	COLOR_WHITE        = "\033[0;37m"
	COLOR_LIGHT_RED    = "\033[1;31m"
	COLOR_LIGHT_GREEN  = "\033[1;32m"
	COLOR_LIGHT_YELLOW = "\033[1;33m"
	COLOR_LIGHT_BLUE   = "\033[1;34m"
	COLOR_LIGHT_PURPLE = "\033[1;35m"
	COLOR_LIGHT_CYAN   = "\033[1;36m"
	COLOR_LIGHT_WHITE  = "\033[1;37m"
	COLOR_INVERT       = "\033[7m"
	COLOR_UNINVERT     = "\033[27m"
)

func (m Model) Init() tea.Cmd {
	return nil
}

func (m Model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd

	// update textview
	m.command, cmd = m.command.Update(msg)

	switch msg := msg.(type) {
	case tea.WindowSizeMsg:
		m.windowWidth, m.windowHeight = msg.Width, msg.Height
	case tea.KeyMsg:
		switch keypress := msg.String(); keypress {
		case ":":
			m.command.Focus()
		case "esc":
			m.command.Reset()
			m.command.Blur()
		case "enter":
			cmd = ProcessCommand(m.command.Value())
			m.command.Reset()
			m.command.Blur()
		case "c", "home":
			if !m.command.Focused() {
				m.dayOffset = 0
			}
		case "j", "down":
			if !m.command.Focused() {
				m.dayOffset += 1
			}
		case "k", "up":
			if !m.command.Focused() {
				m.dayOffset -= 1
			}
		case "h", "left":
		case "l", "right":
		case "g", "PgUp":
			if !m.command.Focused() {
				m.dayOffset += m.windowHeight - 2
			}
		case "G", "PgDn":
			if !m.command.Focused() {
				m.dayOffset -= m.windowHeight - 2
			}
		case "f1":
			m.showDebug = !m.showDebug
		}
	}

	return m, cmd
}

func (m Model) CalendarLineView(view *strings.Builder, lines int) {
	// calculate each line representation
	previousStart := 0
	dayMinutes := 24 * 60

	for line := 0; line < lines; line++ {
		nowTime := time.Now().AddDate(0, 0, m.dayOffset+line)
		now := DateFromTime(nowTime)

		// write style for today
		if line+m.dayOffset == 0 {
			view.WriteString(STYLE_BOLD)
			view.WriteString(COLOR_LIGHT_GREEN)
		}

		// get lengths for line view
		auxCharacterLength := AddPrefixCalendarLine(view, &nowTime)
		length := max(0, m.windowWidth-auxCharacterLength)
		subdivision := dayMinutes / length

		// initialize line characters
		characters := make([]rune, length)
		for i := range characters {
			characters[i] = ' '
		}

		// find relevant entries
		for j := previousStart; j < len(m.calendar.Entries); j++ {
			entry := m.calendar.Entries[j]
			if DateFromTime(entry.Start) == now {
				previousStart = j
				startMinute := entry.Start.Hour()*24 + entry.Start.Minute()
				endMinute := entry.End.Hour()*24 + entry.End.Minute()
				for char := 0; char < length; char++ {
					minute := char * subdivision
					if minute >= startMinute && minute < endMinute {
						characters[char] = '█'
					}
				}
			}
		}

		for _, char := range characters {
			view.WriteRune(char)
		}

		AddSuffixCalendarLine(view)

		// reset style for today
		if line+m.dayOffset == 0 {
			view.WriteString(STYLE_RESET)
		}

		if line != lines-1 && nowTime.Weekday() == time.Sunday {
			view.WriteString("\n")
			line++
		}
	}
}

func (m Model) CommandLineView(view *strings.Builder) {
	view.WriteRune('\n')
	view.WriteString(m.command.View())
}

func (m Model) View() string {
	// begin building the string
	var view strings.Builder

	// render calendar line by line using calendarlineview
	view.WriteString("ultra cool calendar application\n\n")
	m.CalendarLineView(&view, m.windowHeight-4)
	m.CommandLineView(&view)

	// debug option
	if m.showDebug {
		// debug string view
		var debug strings.Builder
		debug.WriteString("debug")
		debug.WriteRune('\n')
		// finally return view
		return debug.String() + view.String()
	} else {
		return view.String()
	}
}
