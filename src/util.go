package main

import (
	"strings"
	"time"
)

const (
	DAY_FORMAT     string = "Mon Jan 02"
	START_FORMAT   string = "Mon Jan 02 15:04"
	END_FORMAT     string = "15:04"
	padLeftString  string = " │"
	padRightString string = "│ "
	padLeftLength  int    = len(padLeftString)
	padRightLength int    = len(padRightString)
)

func AddPrefixCalendarLine(view *strings.Builder, nowTime *time.Time) int {
	timePrefix := nowTime.Format(DAY_FORMAT)
	view.WriteString(timePrefix)
	view.WriteString(padLeftString)
	return padLeftLength + padRightLength + len(timePrefix)
}

func AddSuffixCalendarLine(view *strings.Builder) {
	view.WriteString(padRightString)
	view.WriteRune('\n')
}
